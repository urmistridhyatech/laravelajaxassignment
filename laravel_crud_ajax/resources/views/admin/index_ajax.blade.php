@extends('admin.layout.master')
@section('header','Dashboard')
@section('title','Dashboard')
@section('content')

  <!-- The Modal -->
        <div class="row pt-5 mt-5">
          <div class="col-md-12">
            <div class="card">
              <div class="card-header">
                <h5 class="card-category">All Persons List</h5>
                <h4 class="card-title"> User Stats</h4>
              </div>
              <div class="card-body">
                <div class="table-responsive">
                  <table class="table">
                      <thead class=" text-primary">
                       
                      </thead>
                      <tbody id="tbody">
                        </tbody>
                  </table>
               <!-- 1st part -->
                </div>
              </div>
            </div>
          </div>
        </div>
<!-- The Modal -->
  <div class="modal" id="myModal">
    <div class="modal-dialog">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Modal Heading</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
          {{ Form::open(array('url' => url('/update_user'),'files' => true ,'id'=>'upload_userdata')) }}
            @csrf  
            {!! Form::hidden('id','', array('class' => 'form-control' , 'id' => 'id')) !!}                          
            <center>   
            <img src="" class="img-thumbnail" alt="" id="image"> <br><br></center>
            {!! Form::label('name', '' , array('class'=>'form-lable')) !!}
            {!! Form::text('name','' , array('class' => 'form-control' , 'id' => 'name')) !!}
            <p class="text-danger name"></p>
            <br>

            {!! Form::label('Last Name', '' , array('class'=>'form-lable')) !!}
            {!! Form::text('lastName','', array('class' => 'form-control' , 'id' => 'lastName')) !!}
            <p class="text-danger lastName"></p>  

            <br>
            {!! Form::label('Address', '' , array('class'=>'form-lable')) !!}
            {!! Form::text('address','',array('class' => 'form-control' , 'id' => 'address')) !!}
            <p class="text-danger address"></p>
            <br>

            {!! Form::label('Zipcode', '' , array('class'=>'form-lable')) !!}
            {!! Form::number('zipcode','',array('class' => 'form-control' , 'id' => 'zipcode')) !!}
            <p class="text-danger zipcode"></p>
            <br>

            {!! Form::label('Mobile Number', '' , array('class'=>'form-lable')) !!}
            {!! Form::number('mobileNumber','',array('class' => 'form-control' , 'id' => 'mobileNumber')) !!}
            <p class="text-danger mobileNumber"></p>
            <br>
            
            {!! Form::label('Email', '' , array('class'=>'form-lable')) !!}
            {!! Form::email('email', '' ,array('class' => 'form-control' , 'id' => 'email')) !!}
            <p class="text-danger email"></p>
            <br>


            {!! Form::label('Gender', '' , array('class'=>'form-lable')) !!} &nbsp;&nbsp;
            {{ Form::radio('gender', 'male',null ,['id'=>'male'] ) }}
            {!! Form::label('Male' ,'' , array('class'=>'form-lable')) !!} &nbsp;&nbsp;
            {{ Form::radio('gender', 'female',null ,['id'=>'female']) }}
            {!! Form::label('Female' ,'', array('class'=>'form-lable')) !!}
            
            <br> <br>
            
            {!! Form::label('Profile Image', '' , array('class'=>'form-lable')) !!} <br>
            {!! Form::file('image',array('class' => 'form-control' , 'id' => 'image')) !!}
          </div>
            <center> {!! Form::submit('submit',array('class'=>'btn-submit'  , 'id' => 'submit')); !!}</center>
        
        <!-- Modal footer -->
        <div class="modal-footer">
           {!! Form::close() !!}
        </div>
        
      </div>
    </div>
  </div>  

      <script>
$(document).ready(function(){
  $("#searchData").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#tbody tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});
</script>
@endsection
