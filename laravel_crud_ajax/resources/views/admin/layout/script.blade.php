  <!--   Core JS Files   -->
  
  <script src="js/core/jquery.min.js"></script>
  <script src="js/core/popper.min.js"></script>
  <script src="js/core/bootstrap.min.js"></script>
  <script src="js/plugins/perfect-scrollbar.jquery.min.js"></script>
  <!--  Google Maps Plugin    -->
  <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
  <!-- Chart JS -->
  <script src="js/plugins/chartjs.min.js"></script>
  <!--  Notifications Plugin    -->
  <script src="js/plugins/bootstrap-notify.js"></script>
  <!-- Control Center for Now Ui Dashboard: parallax effects, scripts for the example pages etc -->
  <script src="js/now-ui-dashboard.min.js?v=1.5.0" type="text/javascript"></script><!-- Now Ui Dashboard DEMO methods, don't include it in your project! -->
  <script src="demo/demo.js"></script>
  <script>
    $(document).ready(function() {
      // Javascript method's body can be found in assets/js/demos.js
      demo.initDashboardPageCharts();

    });
  </script>
  <script>
    $(function() {
      $(".equipCatValidation").keydown(function(event) {
         k = event.which;
         l = event.keyCode;
         if ((k >= 96 && k <= 105) || (l >= 48 && l <= 57) || k == 8) {
           if ($(this).val().length == 10) {
             if (k == 8) {
               return true;
             } else {
               event.preventDefault();
               return false;
             }
           }
         } else {
           event.preventDefault();
           return false;
         }

       });

    })
    </script>
