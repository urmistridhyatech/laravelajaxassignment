<div class="sidebar" data-color="orange">
      <!--
        Tip 1: You can change the color of the sidebar using: data-color="blue | green | orange | red | yellow"
    -->
      <div class="logo">
        <a href="http://www.creative-tim.com" class="simple-text logo-mini">
          CT
        </a>
        <a href="http://www.creative-tim.com" class="simple-text logo-normal">
          Creative Tim
        </a>
      </div>
      <div class="sidebar-wrapper" id="sidebar-wrapper">
        <ul class="nav">
          <li>
            <a href="/dashboard">
              <i class="now-ui-icons design_app"></i>
              <p>Dashboard</p>
            </a>
          </li>
          <li>
            <a href="/add_user">
              <i class="now-ui-icons education_atom"></i>
              <p>Add User</p>
            </a>
          </li>
          <li>
            <a href="/profile_view">
              <i class="now-ui-icons users_single-02"></i>
              <p>User Profile</p>
            </a>
          </li>
          <li>
            <a href="/add_state">
              <i class="now-ui-icons users_single-02"></i>
              <p>Add State</p>
            </a>
          </li>
          <li>
            <a href="/add_city">
              <i class="now-ui-icons users_single-02"></i>
              <p>Add City</p>
            </a>
          </li>
          <li>
            <a href="/admin_logout">
              <i class="fas fa-sign-out-alt"></i>
              <p>Logout</p>
            </a>
          </li>
        </ul>
      </div>
    </div>