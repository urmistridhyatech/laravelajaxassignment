@extends('admin.layout.master')
@section('header','Add city')
@section('title','Add city')
@section('content')
		

	{{ Form::open(array('url' => url('/add_city_data'),'files' => true, 'class'=>'card mt-5 w-100','id'=>'city_form')) }}
	<h1 class="font-color" style="text-align: center">ADD CITY</h1>
			@csrf
	     
      






        <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"><p class="text-danger">Update</p></h4>
        </div>
        <div class="modal-body">
          <div class="col-4">
					    <select class="form-control" name="stateName">
   							
    									@foreach ($states as $state)
    							<option value="" selected="{{$state[0]['name']}}"></option>

           						 			<option value="{{$state->name}}">{{$state->name}}</option>
      			  						@endforeach
						</select>
								<span class="text-danger">{{ $errors->first('stateName') }}</span>
					</div>
        </div>
        <div class="col-4">
            					<input type="name" id="name" name="cityname" placeholder="Enter city"><br><br>
               		    		<span class="text-danger">{{ $errors->first('cityname') }}</span>
              		</div>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary ml-5" data-dismiss="modal">UPDATE</button>
        </div>
      </div>
      
    </div>
  </div>
  <form>
				<div class="form-group {{ $errors->has('state') ? 'has-error' : '' }}">
				<div class="row justify-content-around">
			       <div class="col-4">
					    <select class="form-control" name="stateName">
    							<option value="" disabled="" selected="">Select State</option>
   							
    									@foreach ($states as $state)
           						 			<option value="{{$state->name}}">{{$state->name}}</option>
      			  						@endforeach
						</select>
								<span class="text-danger">{{ $errors->first('stateName') }}</span>
					</div>
		
					<div class="col-4">
            					<input type="name" id="name" name="cityname" placeholder="Enter city"><br><br>
               		    		<span class="text-danger">{{ $errors->first('cityname') }}</span>
              		</div>
				</div>
        </form>	     
        		 <input type="submit" class="btn btn-primary ml-5" value="ADD" >
				 <table class="table table-striped">
  						<thead>
						    <tr>
						       <th scope="col">ID</th>
						      <th scope="col">State</th>
						      <th scope="col">City</th>
						      <th scope="col">Action</th>
						    </tr>
  						</thead>
					     

					     @foreach($req1 as $data)
					        <tr>
					          <th>{{$data->id }}</th>
					          <th>{{$data->state_name }}</th>
					           <th>{{$data->city_name }}</th>
					           <th><a href="/update_city/{{$data->id}}" data-toggle="modal" data-target="#myModal" ><i class="fa fa-edit"></i></a><a href="/delete_city/{{$data->id}}"><i class=" fa fa-trash"></i></a></th>
					        </tr>
					     @endforeach


				 </table>
				 {{ $req1->links()  }}
@endsection
                         
		
                    

                          

                      
                          
                     	
                 

		

       

