@extends('user.layout.master')
@section('header','Profile')
@section('content')



<div class="profile-view">
	<center>
	<img src="img/{{$user->profile_image}}" alt="">
	<br><br>
	<table class="card w-50">
		<tbody>
			<tr>
				<td><b>name</b></td>
				<td><b>:</b></td>
				<td>{{ $user->name }}</td>
			</tr>
			<tr>
				<td><b>Last Name</b></td>
				<td><b>:</b></td>
				<td>{{ $user->lastname }}</td>
			</tr>
			<tr>
				<td><b>Gender</b></td>
				<td><b>:</b></td>
				<td>{{ $user->gender }}</td>
			</tr>
      <tr>
        <td><b>Address</b></td>
        <td><b>:</b></td>
        <td>{{ $user->address }}</td>
      </tr>
      <tr>
        <td><b>Zipcode</b></td>
        <td><b>:</b></td>
        <td>{{ $user->zipcode }}</td>
      </tr>
			<tr>
				<td><b>Mobile Number</b></td>
				<td><b>:</b></td>
				<td>{{ $user->mobileNumber }}</td>
			</tr>
			<tr>
				<td><b>Email</b></td>
				<td><b>:</b></td>
				<td>{{ $user->email }}</td>
			</tr>
		</tbody>
	</table>
</center>


	<div class="modal" id="myModal">
                          <div class="modal-dialog">
                            <div class="modal-content">
                            
                              <!-- Modal Header -->
                              <div class="modal-header">
                                <h4 class="modal-title">Update user</h4>
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                              </div>
                              
                              <!-- Modal body -->
                              <div class="modal-body">
                                {{ Form::open(array('url' => url('/update_user_profile'),'files' => true)) }}
                                @csrf  
                                {!! Form::hidden('id', $user->id  , array('class' => 'form-control')) !!}                          
                                <center>   
                                <img src="img/{{$user->profile_image}}" alt=""> <br><br></center>
                                {!! Form::label('name', '' , array('class'=>'form-lable')) !!}
                                {!! Form::text('firstName', $user->name  , array('class' => 'form-control')) !!}
                                <br><br>

                                {!! Form::label('Last Name', '' , array('class'=>'form-lable')) !!}
                                {!! Form::text('lastName', $user->lastName, array('class' => 'form-control')) !!}
                                <br><br>

                                {!! Form::label('Address', '' , array('class'=>'form-lable')) !!}
                                {!! Form::text('address', $user->address,array('class' => 'form-control')) !!}
                                <br> <br>
                                

                                {!! Form::label('Zipcode', '' , array('class'=>'form-lable')) !!}
                                {!! Form::number('zipcode', $user->zipcode,array('class' => 'form-control')) !!}
                                <br> <br>


                                {!! Form::label('Mobile Number', '' , array('class'=>'form-lable')) !!}
                                {!! Form::number('mobileNumber', $user->mobileNumber,array('class' => 'form-control equipCatValidation')) !!}
                                <br> <br>



                                
                                {!! Form::label('Email', '' , array('class'=>'form-lable')) !!}
                                {!! Form::text('email', $user->email,array('class' => 'form-control')) !!}
                                <br><br>

                                {!! Form::label('Gender', '' , array('class'=>'form-lable')) !!} &nbsp;&nbsp;
                                {{ Form::radio('gender', 'male', ($user->gender)=='male'? true : false ) }}
                                {!! Form::label('Male' ,'' , array('class'=>'form-lable')) !!} &nbsp;&nbsp;
                                {{ Form::radio('gender', 'female' ,  ($user->gender)=='female'? true : false ) }}
                                {!! Form::label('Female' ,'', array('class'=>'form-lable')) !!}
                                <br><br>

                                {!! Form::label('Profile Image', '' , array('class'=>'form-lable')) !!} <br><br>
                                {!! Form::file('image',array('class' => 'form-control')) !!}
                              </div>
                             <center> {!! Form::submit('submit',array('class'=>'btn-submit')); !!}</center>
                              <!-- Modal footer -->
                             <br><br>
                              {!! Form::close() !!}
                            </div>
                          </div>
                        </div>
                        <center>
                        <button type="button" data-toggle="modal" data-target="#myModal" class="btn btn-primary">Update</button></center>
</div>
@endsection