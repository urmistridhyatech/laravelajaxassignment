<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => Str::random(10),
            'email' => Str::random(10).'@gmail.com',
            'lastName' => Str::random(10),
            'mobileNumber' => 9999999999,
            'gender' => 'male',

            'profile_image' => Str::random(10),
            'password' => Hash::make('password'),
            'status' => 0,
        ]);
    }
}
