<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/admin','LoginController@admin');
Route::post('/admin_login','LoginController@loginCheck');
Route::post('/ajax_editeUserDataa',"AddStateController@ajax_editeUserDataa")->name('ajax_editeUserDataa');
	

	

Route::group(['middleware' => 'loginMiddleware'],function()
{
	
	//Route::view('/dashboard', 'admin.index')->name('dashboard');		
	Route::get('/dashboard_data','DashboardController@dashboard');
	Route::get('/dashboard','DashboardController@dashboard');
	Route::get('/add_user','AddUserController@addUserForm');
	Route::get('/profile_view','ProfileViewController@profileView');
	Route::get('/delete_user/{id}','AddUserController@DeleteUserData');
	Route::get('/update_user_status/{id}/{status}','AddUserController@updateUserStatus');
	Route::get('/admin_logout','LoginController@logout');

	Route::post('/add_user_data','AddUserController@addUserData');
	Route::get('/update_user/{id}','AddUserController@updateUserData');


	Route::get('/add_state','AddStateController@addState');
	Route::post('/add_state_data','AddStateController@addStateData');
	//Route::post('/update_state/{id}','AddStateController@updateStateData');
	Route::get('/delete_state/{id}','AddStateController@deleteStateData');
	Route::get('/add_city','AddCityController@addCity');
	Route::post('/add_city_data','AddCityController@addCityData');
	Route::get('/update_city/{id}','AddCityController@updatecityData');
	Route::get('/delete_city/{id}','AddCityController@deleteCityData');

});

//Auth::routes();
Route::get('/home', 'DashboardController@userDashboard');
Route::group(['middleware' => 'auth'],function()
{
	Route::get('/','HomeController@index')->name('auth');
	Route::get('/update_user_profile','AddUserController@updateUserProfile');
	Route::get('/user_profile_view','ProfileViewController@userProfileView');
});


