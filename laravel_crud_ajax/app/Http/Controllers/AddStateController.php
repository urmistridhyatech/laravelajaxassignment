<?php

namespace App\Http\Controllers;
use Illuminate\Database\Eloquent\Collection ;
use Illuminate\Http\Request;
use App\State;
use Validator;

class AddStateController extends Controller
{
    public function addState()
    {   
   
        $users = State::Paginate(3);
    	return view('admin.addStateForm',['users' => $users]);
    }
    public function addStateData(Request $stateForm)
    {
    	   
     $validator=Validator::make($stateForm->all(),[
            'stateName' => 'required|unique:state,name',
        ]);

         if($validator->fails())
         {
          
           return redirect("/add_state")->withErrors($validator)->withInput();
         }
         else
          {
           $stateTable = new State();
           $stateTable->id = $stateForm->id;
    	   $stateTable->name = $stateForm->stateName;
    	   $stateTable->save();
    	   return redirect('add_state');
          }
    }
public function ajax_editeUserDataa(Request $request)
    {
        /*
        validate user data using request class
        */
       

      
        // upload image in storage directry and insert in database
      

      //  $oldimage=$request->oldimage;

       

     
       
      

            //echo $userImage;

            $user = state::find($request->id);
            $user->name     = $request->name;
          
            $response       = $user->save();

            echo json_encode(["ans"=>1]);






}

    public function updateStateData(Request $stateForm)
    {
        $validator=Validator::make($stateForm->all(),[
            'stateName1' => 'required|unique:state,name',
            
        
        ]);

         if($validator->fails())
         {
          //return redirect('/add_user')->withErrors($validator)->withInput();
            echo json_encode(["ans"=>"0","error"=>$validator->errors()]);
            
         }

         else{
        
        $state = new State();
        $id = $stateForm->id;
    
        $stateTable = $state::find($id);

        $stateTable->name = $stateForm->stateName1;
    

        $stateTable->save();
        echo json_encode(["ans"=>1]);

    }
  }



    public function deleteStateData($id)
    {
             State::destroy($id);
             return redirect('add_state');
    }

}
